#ifndef carrinho_hpp
#define carrinho_hpp

//Incluindo classes necessárias.
#include "produto.hpp"
#include "estoque.hpp"

//Incluindo bibliotecas necessárias.
#include <vector>
#include <iostream>
#include <string>
#include <stdbool.h>

using namespace std;

//Classe carrinho que herda classe estoque.
class Carrinho:public Estoque{
private:
    vector <Produto*> produtos_list; //vetor de produtos, vulgo carrinho.
    double preco; //preco total.
    string pagamento; //metodo de pagamento.
    int t_produtos_list; //tamanho do vetor de produtos, vulgo carrinho.
public:

    //construtor-->
    Carrinho();

    //Destrutor-->
    ~Carrinho();
    //Getters e Setters-->
    //Getters:
    void get_produtos_list();
    vector <Produto*> return_produtos_list();
    double get_preco();
    string get_pagamento();
    int get_t_produtos_list();
    //setters:
    void set_preco();
    void set_pagamento();
    void set_t_produtos_list();
    

    //outros metodos-->
    void cadastra_produto();
    void rmv_prod();
    bool realizar_pagamento();
};

#endif