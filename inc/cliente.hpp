#ifndef cliente_hpp
#define cliente_hpp

//Incluindo bibliotecas necessárias.
#include <iostream>
#include <string>

using namespace std;

//Classe cliente:
class Cliente{
private:
  string nome,email,cpf;
public:

  //construtor:
  Cliente();
  //destrutor:
  ~Cliente();

  //metodos

  //getters e setters:
  //getters:
  string get_nome();
  string get_email();
  string get_cpf();
  //setters:
  void set_nome(string nome);
  void set_email(string email);
  void set_cpf(string cpf);

  //outros metodos:
};

#endif