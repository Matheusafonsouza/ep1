#ifndef produto_hpp
#define produto_hpp

//Incluindo bibliotecas necessárias.
#include <iostream>
#include <string>
#include <vector>

using namespace std;

//Classe produto
class Produto{
private:
    int quantidade,t_categorias; //Quantidade de produtos
    double valor; //Valor do produto
    string nome,codigo;
    vector <string> categorias; 
public:

    //construtor-->
    Produto();
    Produto(string nome,vector<string> categorias,double valor,int quantidade,string codigo);

    //Destrutor-->
    ~Produto();
    //Getters e Setters-->
    //Getters:
    int get_quantidade();
    string get_codigo();
    double get_valor();
    string get_nome();
    string get_categorias(int indice);
    int get_t_categorias();
    vector <string> get_vector_categorias();
    //setters:
    void set_quantidade(int quantidade);
    void set_codigo(string codigo);
    void set_valor(double valor);
    void set_nome(string nome);
    void set_categorias(vector <string> categoria);
    void set_t_categorias();

    //outros metodos-->
};

#endif