#ifndef recomendacao_hpp
#define recomendacao_hpp

//Incluindo classes necessárias.
#include "produto.hpp"
//Incluindo bibliotecas necessárias.
#include <iostream>
#include <string>
#include <vector>
#include <stdbool.h>


//Classe recomendação.
class Recomendacao{
private:
    int t_categorias,t_lista;
    string nome;
    vector <Produto*> lista;
    vector <string> categorias;
    vector <Produto*> estoque;
    vector <int> conta;
public:
    //Construtor.
    Recomendacao();
    Recomendacao(vector <Produto*> lista);
    Recomendacao(string nome);
    //Destrutor.
    ~Recomendacao();

    //Setters.
    void set_tcategorias();
    void set_t_lista();

    //Outros métodos.
    void concatena(string nome);
    void carrega_lista_produtos(vector <Produto*> lista);
    void fecha_arquivo();
    void carrega_arquivo();
    bool verifica_arquivo();
    void define_categorias_list();
    void ordena_categorias();
    void carrega_estoque(vector <Produto*> estoque);
    void recomenda();
};


#endif