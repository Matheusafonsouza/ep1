#ifndef socio_hpp
#define socio_hpp

//Incluindo classes necessárias.
#include "cliente.hpp"

//Incluindo bibliotecas necessárias.
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <stdbool.h>

//Classe socio que herda classe cliente.
class Socio:public Cliente{
private:
  string sexo,endereco,nasc,telefone;
  vector <Socio*> list_socio;
  int t_socio;
public:

  //construtor-->
  Socio();
  Socio(string nome,string sexo,string endereco,string email,string nasc,string cpf,string telefone);
  //destrutor-->
  ~Socio();

  //metodos-->

  //getters e setters
  string get_sexo();
  string get_endereco();
  string get_telefone();
  string get_nasc();
  void set_nasc(string nasc);
  void set_sexo(string sexo);
  void set_endereco(string endereco);
  void set_telefone(string telefone);
  void set_t_socio();
  void set_socio(); 
  
  //Outros metodos
  bool verifica_socio();
  void cadastra_socio();
  bool verifica_cpf();
  void fechar_socio();
};

#endif