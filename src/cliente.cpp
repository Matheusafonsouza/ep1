#include <iostream>
#include <string>

#include "cliente.hpp"

//construtor
Cliente::Cliente(){
  set_nome("");
  set_cpf("");
  set_email("");
}

//destrutor
Cliente::~Cliente(){
}

//getters
string Cliente::get_nome(){
  return this->nome;
}
string Cliente::get_email(){
  return this->email;
}
string Cliente::get_cpf(){
  return this->cpf;
}

//setters
void Cliente::set_nome(string nome){
  this->nome=nome;
} 
void Cliente::set_email(string email){
  this->email=email;
}
void Cliente::set_cpf(string cpf){
  this->cpf=cpf;
}
