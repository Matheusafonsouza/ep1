#include <iostream>
#include <string>
#include "cliente.hpp"
#include "socio.hpp"
#include <fstream>
#include <stdbool.h>

//construtor
Socio::Socio(){
  set_nome("");
  set_endereco("");
  set_sexo("");
  set_email("");
  set_cpf("");
  set_telefone("");
  set_nasc("");
  set_t_socio();
  set_socio();
}

//construtor com parametros
Socio::Socio(string nome,string sexo,string endereco,string email,string nasc,string cpf,string telefone){
  set_nome(nome);
  set_endereco(endereco);
  set_sexo(sexo);
  set_email(email);
  set_cpf(cpf);
  set_telefone(telefone);
  set_nasc(nasc);
}

//destrutor
Socio::~Socio(){
}

//getters
string Socio::get_sexo(){
  return this->sexo;
}
string Socio::get_endereco(){
  return this->endereco;
}
string Socio::get_nasc(){
  return this->nasc;
}
string Socio::get_telefone(){
  return this->telefone;
}

//setters
void Socio::set_sexo(string sexo){
  this->sexo=sexo;
} 
void Socio::set_endereco(string endereco){
  this->endereco=endereco;
}
void Socio::set_telefone(string telefone){
  this->telefone=telefone;
}
void Socio::set_nasc(string nasc){
  this->nasc=nasc;
}

//outros metodos-->

//determina tamanho do vetor de socios
void Socio::set_t_socio(){
  ifstream list_socios;
  string t_socios;
  list_socios.open("arquivos/socios.txt");
  if(list_socios.is_open()){
    getline(list_socios,t_socios);
    this->t_socio=atoi(t_socios.c_str());
    list_socios.close();
  }else{
    this->t_socio=0;
  }
}

//carrega arquivo txt socio
void Socio::set_socio(){
    if(verifica_socio()==true){
        string nome,lixo,email,sexo,endereco,cpf,nasc,telefone;
        ifstream list_socio;
        list_socio.open("arquivos/socios.txt",ios::in);    
        getline(list_socio,lixo);
        Socio* meu_socio;
        for(int i=0;i<this->t_socio;i++){
            getline(list_socio,nome);
            getline(list_socio,email);
            getline(list_socio,sexo);
            getline(list_socio,endereco);
            getline(list_socio,cpf);
            getline(list_socio,nasc);
            getline(list_socio,telefone);
            meu_socio=new Socio(nome,sexo,endereco,email,nasc,cpf,telefone);
            this->list_socio.push_back(meu_socio);
        }
        list_socio.close();
    }
    if(verifica_socio()==false){
    }
}

//verifica se arquivo txt socio existe
bool Socio::verifica_socio(){
    ifstream list_socio;
    list_socio.open("arquivos/socios.txt",ios::in);
    if(list_socio.is_open()){
        list_socio.close();
        return true;
    }
    else{
        return false;
    }
}

//cadastra socio no vetor
void Socio::cadastra_socio(){
  string cpf,nome,sexo,endereco,nasc,telefone,email;
  cout<<"Digite seu cpf:";
  getline(cin,cpf);
  set_cpf(cpf);
  for(int i=0;i<this->t_socio;i++){
    if(cpf==this->list_socio[i]->get_cpf()){
      cout<<"Socio já existente"<<endl;
      cout<<"Aperte enter para continuar..."<<endl;
      set_nome(this->list_socio[i]->get_nome());
      set_sexo(this->list_socio[i]->get_sexo());
      set_endereco(this->list_socio[i]->get_endereco());
      set_email(this->list_socio[i]->get_email());
      set_nasc(this->list_socio[i]->get_nasc());
      set_telefone(this->list_socio[i]->get_telefone());
      return;
    }
  }
  cout<<endl<<"Socio não encontrado. Iniciando cadastrado...";
  cout<<endl<<"Nome:";
  getline(cin,nome);
  cout<<endl<<"Sexo:";
  getline(cin,sexo);
  cout<<endl<<"Endereço:";
  getline(cin,endereco);
  cout<<endl<<"Email:";
  getline(cin,email);
  cout<<endl<<"Nascimento:";
  getline(cin,nasc);
  cout<<endl<<"Telefone:";
  getline(cin,telefone);
  set_nome(nome);
  set_sexo(sexo);
  set_endereco(endereco);
  set_email(email);
  set_nasc(nasc);
  set_telefone(telefone);
  Socio *meu_socio=new Socio(nome,sexo,endereco,email,nasc,cpf,telefone);
  this->list_socio.push_back(meu_socio);
  cout<<"Socio cadastrado com sucesso!"<<endl;
  this->t_socio++;
}

//salva arquivo txt de socios
void Socio::fechar_socio(){
    if(verifica_socio()==true||this->list_socio.size()!=0){
        ofstream list_socio;
        list_socio.open("arquivos/socios.txt",ios::out);
        list_socio<<this->t_socio<<endl;
        for(int i=0;i<this->t_socio;i++){
            list_socio<<this->list_socio[i]->get_nome()<<endl;
            list_socio<<this->list_socio[i]->get_email()<<endl;
            list_socio<<this->list_socio[i]->get_sexo()<<endl;
            list_socio<<this->list_socio[i]->get_endereco()<<endl;
            list_socio<<this->list_socio[i]->get_cpf()<<endl;
            list_socio<<this->list_socio[i]->get_nasc()<<endl;
            list_socio<<this->list_socio[i]->get_telefone()<<endl;
        }
        list_socio.close();
    }else{
    }
}

//verifica se o cpf é de um sócio ou não.
bool Socio::verifica_cpf(){
  string cpf;
  cout<<"Digite seu cpf: ";
  getline(cin,cpf);
  for(int i=0;i<this->t_socio;i++){
    if(cpf==this->list_socio[i]->get_cpf()){
      cout<<"Socio já existente"<<endl;
      set_cpf(cpf);
      cout<<"Aperte enter para continuar..."<<endl;
      return true;
    }
  }
  cout<<"Socio não existente"<<endl;
  cout<<"Aperte enter para continuar..."<<endl;
  return false;
}