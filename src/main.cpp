#include <iostream>
#include "cliente.hpp"
#include "socio.hpp"
#include <vector>
#include <string>
#include "carrinho.hpp"
#include <fstream>
#include <stdbool.h>
#include "estoque.hpp"
#include "produto.hpp"
#include "recomendacao.hpp"

using namespace std;

int main(){

  //Vector para arquivo de socios
  //declaração de variaveis
  int decisao=0;
  char escolha,op;
  string nome,email,sexo,endereco,cpf,nasc,telefone;
  bool final=true;
  Socio* meu_socio=new Socio();
  do{

    //Objeto cliente para inicio de operação
    

    //printar menu
    system("clear");
    cout<<"Bem vindo ao programa da LOJA!"<<endl;
    cout<<"1. Modo Venda."<<endl;
    cout<<"2. Modo Estoque."<<endl;
    cout<<"3. Modo Recomendação."<<endl;
    cout<<"0. Fechar programa."<<endl;
    
    //escolha menu.
    cin>>op;
    cin.ignore();
    system("clear");

    //switch para escolha do menu.
    switch(op){
    //escolha venda.
    case '1':
      {
      meu_socio->cadastra_socio();
      getchar();

      Carrinho *meu_carrinho=new Carrinho();


      do{
        decisao=0;
        system("clear");
        cout<<"BEM VINDO AO MENU DE VENDA!"<<endl;
        cout<<"1. Colocar produto no carrinho."<<endl;
        cout<<"2. Retirar produto do carrinho."<<endl;
        cout<<"3. Printar carrinho."<<endl;
        cout<<"4. Printar estoque de produtos."<<endl;
        cout<<"5. Finalizar operação"<<endl;
        cout<<"0. Voltar ao menu."<<endl;

        cin>>escolha;
        system("clear");
        switch(escolha){
        case '1':
          {
          meu_carrinho->cadastra_produto();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '2':
          {
          meu_carrinho->rmv_prod();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '3':
          {
          meu_carrinho->get_produtos_list();
          cin.ignore();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '4':
          {
          meu_carrinho->get_estoque_de_produtos();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '5':
          {
          final=meu_carrinho->realizar_pagamento();
          if(final==false){
            decisao=0;
          }else{
            decisao=1;
            Recomendacao *minha_recomendacao=new Recomendacao(meu_carrinho->return_produtos_list());
            minha_recomendacao->concatena(meu_socio->get_cpf());
            minha_recomendacao->define_categorias_list();
            minha_recomendacao->fecha_arquivo();
            getchar();
          }
          break;
          }
        case '0':
          {
          break;
          }
        default:
          {
          cout<<"Opção inválida! Aperte enter para voltar."<<endl;
          cin.ignore();
          getchar();
          break;
          }
        }

      
      }while(escolha!='0' && decisao==0);
      meu_carrinho->fechar_estoque();
      meu_carrinho->fechar_categorias(); 
      break;
      }
    //escolha estoque
    case '2': 
      {
      //cria objeto da classe estoque para operações
      Estoque *estoque_de_produtos=new Estoque();
      //dowhile para permanecer no menu de estoque
      do{
        system("clear");
        //printa menu de estoque
        cout<<"BEM VINDO AO MENU DE ESTOQUE!"<<endl;
        cout<<"1. Cadastrar produto."<<endl;
        cout<<"2. Cadastrar categoria."<<endl;
        cout<<"3. Printar estoque."<<endl;
        cout<<"4. Printar categorias."<<endl;
        cout<<"5. Alterar quantidade de um produto"<<endl;
        cout<<"0. Voltar ao menu."<<endl;
        //escolha do menu de estoque
        cin>>escolha; 
        //switch para escolha
        system("clear");
        switch(escolha){
        case '1':
          {
          //cadastra produto no vector de produtos
          estoque_de_produtos->cadastra_produto();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '2':
          {
          //cadastra categoria no vector de categoria
          estoque_de_produtos->set_categoria();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '3':
          {
          //printa estoque de produtos
          estoque_de_produtos->get_estoque_de_produtos();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '4':
          {
          //printa categorias
          estoque_de_produtos->get_categorias();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '5':
          {
          estoque_de_produtos->altera_quantidade();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '0':
          {
          break;
          }
        default:
          {
          cout<<"Opção inválida! Aperte enter para voltar."<<endl;
          getchar();
          break;
          }
        }

      }while(escolha!='0');
      //salva arquivo txt do estoque e das categorias
      estoque_de_produtos->fechar_estoque();
      estoque_de_produtos->fechar_categorias(); 
      
      break;
      }
    //escolha recomendação
    case '3':
      {
      decisao=meu_socio->verifica_cpf();
      getchar();
      if(decisao==true){
        Estoque *meu_estoque=new Estoque();
        Recomendacao *minha_recomendacao = new Recomendacao(meu_socio->get_cpf());
        minha_recomendacao->carrega_estoque(meu_estoque->return_estoque());
        minha_recomendacao->recomenda();
        getchar();
      }
      else{
        
      }
      break;
      }
    case '0':
      {
      meu_socio->fechar_socio();
      return 0;
      }
    default:
      {
      cout<<"Opção inválida!"<<endl<<"Aperte qualquer tecla para voltar ao menu."<<endl;
      getchar();
      break;
      }
    }

  }while(op!='0');
  return 0;
}

