#include "produto.hpp"
#include <iostream>
#include <string>

//construtor
Produto::Produto(){
    set_quantidade(0);
    set_codigo("");
    set_valor(0);
    set_nome("");
}

//construtor com parametros
Produto::Produto(string nome,vector<string> categorias,double valor,int quantidade,string codigo){
    set_quantidade(quantidade);
    set_codigo(codigo);
    set_valor(valor);
    set_nome(nome);
    set_categorias(categorias);
    set_t_categorias();
}

//destrutor
Produto::~Produto(){}

//retorna quantidade do produto
int Produto::get_quantidade(){
    return this->quantidade;
}
//retorna codigo do produto
string Produto::get_codigo(){
    return this->codigo;
}
//retorna valor do produto
double Produto::get_valor(){
    return this->valor;
}
//retorna nome do produto
string Produto::get_nome(){
    return this->nome;
}
//retorna categoria do produto
string Produto::get_categorias(int indice){
    return this->categorias[indice];
}

vector <string> Produto::get_vector_categorias(){
    return this->categorias;
}

//define quantidade do produto
void Produto::set_quantidade(int quantidade){
    this->quantidade=quantidade;
}
//define codigo do produto
void Produto::set_codigo(string codigo){
    this->codigo=codigo;
}
//define valor do produto
void Produto::set_valor(double valor){
    this->valor=valor;
}
//define nome do produto
void Produto::set_nome(string nome){
    this->nome=nome;
}
//define categoria do produto
void Produto::set_categorias(vector <string> categorias){
    this->categorias=categorias;
}

//retorna quantidade do vector de categorias.
int Produto::get_t_categorias(){
    return this->t_categorias;
}

//Decide o tamanho do vector de categorias.
void Produto::set_t_categorias(){
    this->t_categorias=int(categorias.size());
}