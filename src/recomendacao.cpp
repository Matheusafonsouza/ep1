#include "recomendacao.hpp"
#include "produto.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <stdbool.h>
#include <cstdlib> 

Recomendacao::Recomendacao(){
}

Recomendacao::Recomendacao(string nome){
    concatena(nome);
    carrega_arquivo();
    set_t_lista();
    set_tcategorias();
    
}

Recomendacao::Recomendacao(vector <Produto*> lista){
    carrega_arquivo();
    carrega_lista_produtos(lista);
    set_t_lista();
    set_tcategorias();
}
Recomendacao::~Recomendacao(){}

void Recomendacao::set_tcategorias(){
    this->t_categorias=int(this->categorias.size());
}

void Recomendacao::ordena_categorias(){
    if(this->t_categorias==0){
        cout<<"Não há recomendações!"<<endl;
    }
    else{
        for(int i=0;i<this->t_categorias;i++){
            for(int j=0;j<this->t_categorias;j++){
                if(this->conta[i]<this->conta[j]){
                    int index=this->conta[j];
                    this->conta[j]=this->conta[i];
                    this->conta[i]=index;
                    string categoria=this->categorias[j];
                    this->categorias[j]=this->categorias[i];
                    this->categorias[i]=categoria;
                }
            }
        }
    }
}

void Recomendacao::concatena(string cpf){
    string txt,final=".txt";
    txt=cpf+final;
    this->nome=txt;
}

void Recomendacao::carrega_lista_produtos(vector <Produto*> lista){
    this->lista=lista;
}

void Recomendacao::set_t_lista(){
    this->t_lista=int(this->lista.size());
}

void Recomendacao::carrega_estoque(vector <Produto*> estoque){
    this->estoque=estoque;
}

void Recomendacao::define_categorias_list(){
    for(int i=0;i<this->t_lista;i++){
        for(int j=0;j<this->lista[i]->get_t_categorias();j++){
            set_tcategorias();
            if(this->t_categorias==0){
                this->categorias.push_back(this->lista[i]->get_categorias(j));
                this->conta.push_back(1);
                this->t_categorias++;
            }
            else{
                int index=0;
                for(int q=0;q<this->t_categorias;q++){
                    if(this->categorias[q]==this->lista[i]->get_categorias(j)){
                        this->conta[q]++;
                        index++;
                    }
                }
                if(index==0){
                    this->categorias.push_back(this->lista[i]->get_categorias(j));
                    this->conta.push_back(1);
                    this->t_categorias++;
                }
            }
        }
    }
    
}


void Recomendacao::fecha_arquivo(){
    ofstream arquivo;
    arquivo.open("arquivos/"+this->nome);
    arquivo<<this->t_categorias<<endl;
    for(int i=0;i<this->t_categorias;i++){
        arquivo<<categorias[i]<<endl;
        arquivo<<conta[i]<<endl;
    }    
    arquivo.close();
}

//carrega txt estoque
void Recomendacao::carrega_arquivo(){
    if(verifica_arquivo()==true){
        string categoria,tamanho,quantidade;
        ifstream arquivo;
        int t_tamanho,quant;
        arquivo.open("arquivos/"+this->nome);
        getline(arquivo,tamanho);
        t_tamanho=atoi(tamanho.c_str());
        for(int i=0;i<t_tamanho;i++){
            vector <string> categorias;
            getline(arquivo,categoria);
            getline(arquivo,quantidade);
            quant=atoi(quantidade.c_str());
            this->categorias.push_back(categoria);
            this->conta.push_back(quant);
        }
        arquivo.close();
    }
    if(verifica_arquivo()==false){
    }
}

//verifica existência do txt estoque
bool Recomendacao::verifica_arquivo(){
    ifstream arquivo;
    arquivo.open("arquivos/"+this->nome);
    if(arquivo.is_open()){
        arquivo.close();
        return true;
    }
    else{
        return false;
    }
}

void Recomendacao::recomenda(){
    ordena_categorias();
    int contador=0;
    cout<<"Produtos recomendados: "<<endl;
    for(int i=this->t_categorias-1;i>=0;i--){
        contador++;
        vector <string> recomendados;
        for(int j=0;j<int(this->estoque.size());j++){
            for(int q=0;q<int(this->estoque[j]->get_t_categorias());q++){
                if(this->categorias[i]==this->estoque[j]->get_categorias(q)){    
                    recomendados.push_back(this->estoque[j]->get_nome());
                }
            }
        }
        if(contador==1){
            if(int(recomendados.size()<5)){
                for(int w=0;w<int(recomendados.size());w++){
                    int tamanho=int(recomendados.size());
                    int randomizado=rand()%tamanho;
                    cout<<recomendados[randomizado]<<endl;
                    recomendados.erase(recomendados.begin()+randomizado);
                }
            }
            else{
                for(int w=0;w<5;w++){
                    int tamanho=int(recomendados.size());
                    int randomizado=rand()%tamanho;
                    cout<<recomendados[randomizado]<<endl;
                    recomendados.erase(recomendados.begin()+randomizado);
                }
            }
        }
        if(contador==2){
            if(int(recomendados.size()<3)){
                for(int w=0;w<int(recomendados.size());w++){
                    int tamanho=int(recomendados.size());
                    int randomizado=rand()%tamanho;
                    cout<<recomendados[randomizado]<<endl;
                    recomendados.erase(recomendados.begin()+randomizado);
                }
            }
            else{
                for(int w=0;w<3;w++){
                    int tamanho=int(recomendados.size());
                    int randomizado=rand()%tamanho;
                    cout<<recomendados[randomizado]<<endl;
                    recomendados.erase(recomendados.begin()+randomizado);
                }
            }
        }
        if(contador==3){
            if(int(recomendados.size()<2)){
                for(int w=0;w<int(recomendados.size());w++){
                    int tamanho=int(recomendados.size());
                    int randomizado=rand()%tamanho;
                    cout<<recomendados[randomizado]<<endl;
                    recomendados.erase(recomendados.begin()+randomizado);
                }
            }
            else{
                for(int w=0;w<2;w++){
                    int tamanho=int(recomendados.size());
                    int randomizado=rand()%tamanho;
                    cout<<recomendados[randomizado]<<endl;
                    recomendados.erase(recomendados.begin()+randomizado);   
                }
            }
        }
    }
}