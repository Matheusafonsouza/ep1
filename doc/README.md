# EP1 Orientação a Objeto:
## Aluno: Matheus Afonso de Souza.
## Matrícula: 18/0127641.

## Bibliotecas necessárias:

iostream, vector, string, fstream, stdbool.h, cstdlib.

## Função Venda:

Ao iniciar a função venda, será requisitado o cpf do cliente (no formado xxx.xxx.xxx-xx) em questão para, deste modo, verificar se ele já é sócio ou não.
Caso o cliente seja sócio, o programa seguirá normalmente para o menu de venda. Caso contrário, o programa requisitará os dados do cliente para, assim, tornar-lo sócio da loja (data de nascimento no formado xx/xx/xxxx).

Em seguida, será printado o menu de Venda com as opções:

1. Colocar produto no carrinho:

Será requisitado o nome do produto e caso ele exista, será acrescentada 1 unidade dele no carrinho.

2. Retirar produto do carrinho:

Será requisitado o nome do produto e caso ele exista no carrinho,será completamente retirado.

3. Printar carrinho:

Printa os produtos do carrinho de compras, mostrando a quantidade, nome e categorias.

4. Printar estoque de produtos:

Printa os produtos do estoque com todos seus dados.

5. Finalizar operação:

Seleciona forma de pagamento e finaliza compra.
Caso a quantidade de certo produto seja maior que sua quantidade no estoque, a operação é cancelada.
Caso ocorra tudo corretamente, será gerado o arquivo txt do sócio com o número de compras das categorias para a função de recomendação.

6. Voltar ao menu:

Volta ao menu para seleção de função.             

## Função Estoque:

Ao selecionar a função estoque, será printado o menu de estoque com as seguintes funções:

1. Cadastrar produto:

Será requisitado o nome e será verificado se ele já existe dentro do estoque. Caso exista, será finalizada a opção de cadastrar e o programa voltará ao menu de estoque. Caso não exista, será requisitado o número de categorias e se todas elas existirem, o resto dos dados serão requisitados e, assim, o produto será cadastrado no estoque.

2. Cadastrar categoria:

Será requisitado o nome da categoria. Caso exista, será finalizada a opção de cadastrar e o programa voltará ao menu de estoque. Caso não exista, será inserido dentro das categorias.

3. Printar estoque:

Printa os produtos do estoque com todos seus dados.

4. Printar categorias:

Printa as categorias disponíveis.

5. Alterar quantidade de um produto:

Requisita o nome do produto. Caso exista, requisita a nova quantidade dele. Caso não exista, retorna ao menu de estoque.

6. Voltar ao menu:

Volta ao menu para seleção de função.             

## Função Recomendação:

Ao escolher a função de recomendação, será requisitado o cpf do sócio para saber se ele possui o arquivo com o histórico de compras de categorias.

Caso não possua, o programa volta ao menu de seleção.
Caso possua, o programa printará produtos recomendados para a próxima compra do sócio.